package com.example.demo.repository;

import java.util.List;

import org.aspectj.weaver.tools.Trace;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long>{
	
	@Query(value = " select u.id userId, u.username, r.id reportId,r.created_date,r.motel_id motelId,r.reason "
			+ "from report r join user u on u.id=r.user_id where 1=1 and r.motel_id= :motelId "
			
			,nativeQuery = true
		
			)
	List<Object[]> doSearch(@Param("motelId") String motelId);

}
