package com.example.demo.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.ERole;
import com.example.demo.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	Optional<Role> findByName(ERole name);
	
	@Query( value = "SELECT r.* "
			+ " FROM Role r join User_Role ur on r.id = ur.role_id"
			+ " WHERE ur.user_id = :user_id", nativeQuery = true)
	Set<Role> findRoleByUser(@Param("user_id") Long id);

}
