package com.example.demo.repository;

import java.util.List;

import com.example.demo.dto.searchDto.MotelSearchClientDTO;

public interface MotelRepositoryCustoms {
 public List<Object[]> searchMotelClient(MotelSearchClientDTO clientDTO);
}
