package com.example.demo.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.example.demo.dto.ParamSearchUserDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.dto.UserResponseDTO;
import com.example.demo.payload.request.SetPasswordRequest;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.services.IFileStorageService;
import com.example.demo.services.IUserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	IUserService IUserService;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	IFileStorageService storageService;
	
	@GetMapping("/details/{id}")
	//@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		try {
			UserDTO userDTO = IUserService.getUserById(id);
			String url = MvcUriComponentsBuilder
					.fromMethodName(FilesController.class, "getFile", userDTO.getAvatar().toString()).build().toString();
			userDTO.setAvatar(url);
			return new ResponseEntity<>(userDTO, HttpStatus.OK);
		} catch (EntityNotFoundException e) {
			return new ResponseEntity<>(new MessageResponse("Not Found User"), HttpStatus.OK);
		}
			
	}

	@PutMapping("/update/{id}")
//	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @ModelAttribute UserDTO userDTO) {
		userDTO.setId(id);
		String message =  IUserService.updateUser(userDTO);
		return new ResponseEntity<>(new MessageResponse(message), HttpStatus.OK);
	}

	@PutMapping("/status/{id}")
//	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> changStatus(@PathVariable("id") Long id, @RequestBody UserDTO userDTO) {
		userDTO.setId(id);
		String message =  IUserService.changeStatus(userDTO);
		return new ResponseEntity<>(new MessageResponse(message), HttpStatus.OK);
	}
	
	@PutMapping("/changePassword/{id}")
//	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<?> changT(@PathVariable("id") Long id, @RequestBody UserDTO userDTO) {
		userDTO.setId(id);
		String message = IUserService.changePassword(userDTO);
		return new ResponseEntity<>(new MessageResponse(message), HttpStatus.OK);
	}
	
	@PostMapping("/forgotPassword")
	public ResponseEntity<?> forgotPassword(@RequestBody UserDTO dto) {
		return new ResponseEntity<>(IUserService.forgotPassword(dto.getEmail()), HttpStatus.OK);
	}
	@GetMapping("/resetPassword")
	public ResponseEntity<?> resetPassword( @RequestParam("token") String token) {
		String message = IUserService.validatePasswordToken(token);
		if(message != null) {
			return new ResponseEntity<>(message,HttpStatus.OK);
		}
		return new ResponseEntity<>(token, HttpStatus.OK);
	}
	@PostMapping("/resetPassword/setNewPassword")
	public ResponseEntity<?> setNewPassword(@ModelAttribute SetPasswordRequest request) {
		String message = IUserService.resetPassword(request);
		return new ResponseEntity<>(message,HttpStatus.OK);
	}
	
	////////////////////////////////////////////////////////////////////////////////==== 90

	@PostMapping("/search")
	//@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> searchUser(@RequestBody ParamSearchUserDTO dto, Pageable pageable) {
		Page<UserResponseDTO> result = IUserService.doSearch(dto, pageable);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	

	@DeleteMapping("/delete/{id}")
	//@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
		IUserService.deleteUser(id);
		return ResponseEntity.ok("Xóa Thành Công");
	}
	
	

}
