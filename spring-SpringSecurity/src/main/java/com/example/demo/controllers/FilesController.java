package com.example.demo.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.example.demo.entity.FileInfo;
import com.example.demo.services.IFileStorageService;



@CrossOrigin(value = "*")
@RestController
@RequestMapping("/api/image")
public class FilesController {
	
	@Autowired
	IFileStorageService storageService;
	
	
//	@PostMapping("/uploadMultipleFiles")
//    public ResponseEntity<MessageResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//		String message = "";
//       try {
//    	   Arrays.asList(files).stream()
//           .map(file -> uploadFile(file))
//           .collect(Collectors.toList());
//    	   message = "Uploaded the file successfully";
//    	   return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(message));
//	} catch (Exception e) {
//		message = "Could not upload file";
//       	return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body( new MessageResponse(message+e.getMessage()));
//	}
//       
//    }
	
	@GetMapping("/files")
	public ResponseEntity<List<FileInfo>> getListFiles(){
		List<FileInfo> fileInfos = storageService.loadAll().map( path -> {
			String filename = path.getFileName().toString();
			String url = MvcUriComponentsBuilder
					.fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
			return new FileInfo(filename, url);
		}).collect(Collectors.toList());
		
		return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
	}
	
	
	@GetMapping("/files/{filename:.+}")
	public ResponseEntity<Resource> getFile(@PathVariable String filename){
		Resource file = storageService.load(filename, "avatar");
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+file.getFilename() +"\"").body(file);
	}
//	@DeleteMapping("/deleteAll")
//	public ResponseEntity<ResponseMessage> deleteAllFiles() {
//		try {
//			storageService.deleteAll();
//		} catch (Exception e) {
//			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not delete All Files"));
//		}
//		return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Delete All File Successfully"));
//	}
	
	
}
