package com.example.demo.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.ERole;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.entity.UserRole;
import com.example.demo.payload.request.LoginRequest;
import com.example.demo.payload.request.SignupRequest;
import com.example.demo.payload.response.JwtResponse;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.UserRoleRepository;
import com.example.demo.security.jwt.JwtUtils;
import com.example.demo.services.IFileStorageService;
import com.example.demo.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRoleRepository userRoleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	IFileStorageService storageService;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		if (userDetails.getStatus() == 1) {
			String jwt = jwtUtils.generateJwtToken(authentication);
			List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
					.collect(Collectors.toList());

			return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
					userDetails.getEmail(), roles));
		}else {
			return ResponseEntity.ok(new MessageResponse("User is locked"));
		}

	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@RequestBody SignupRequest signUpRequest
//			,@RequestParam("file") MultipartFile file
			) {
//		String path = null;
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.ok(new MessageResponse("Rất tiếc! Tên tài khoản đã được người khác sử dụng"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.ok(new MessageResponse("Rất tiếc! Email đã được dùng để đăng ký trước đó"));
		}
		
		// Create new user's account
		User user = new User(signUpRequest.getUsername(), passwordEncoder.encode(signUpRequest.getPassword()),
				signUpRequest.getEmail(), signUpRequest.getFullname(),
				signUpRequest.getBirthday(), signUpRequest.getAddress(), signUpRequest.getPhone(),
				null, signUpRequest.getStatus(),signUpRequest.getGender());
		
		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();
		if (strRoles == null) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Role cannot be empty."));
		}
		for (String r : strRoles) {
			switch (r) {
			case "admin":
				Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new NoSuchElementException());
				roles.add(adminRole);
				break;
			case "user":
				Role userRole = roleRepository.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new NoSuchElementException());
				roles.add(userRole);
				break;

			default:
				return ResponseEntity.badRequest().body(new MessageResponse("Error: Role is not found."));
			}
		}
		user.setAvatar("avatar.jpg");
		user.setStatus(1);
		userRepository.save(user);
		Long userId = userRepository.findOneByUsername(signUpRequest.getUsername()).getId();
		for( Role r : roles) {
			UserRole ur = new UserRole(userId, r.getId());
			userRoleRepository.save(ur);
		}

		return ResponseEntity.ok(new MessageResponse("Success!"));
	}

}
