package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.searchDto.ReportResponDTO;
import com.example.demo.services.ReportService;
import com.example.demo.util.DataUtil;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/report")
public class ReportController {
	
	@Autowired
	private ReportService reportService;
	
	
	@PostMapping(value = "doSearch")
	public ResponseEntity<List<ReportResponDTO>> doSearch(@RequestBody  ReportResponDTO dto){
		return new ResponseEntity<>(reportService.doSearch(DataUtil.safeToString(dto.getMotelId())),HttpStatus.OK);
	}

}
