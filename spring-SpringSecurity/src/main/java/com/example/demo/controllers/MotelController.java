package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.dto.MotelDTO;
import com.example.demo.dto.searchDto.MotelResponDTO;
import com.example.demo.dto.searchDto.MotelSearchClientDTO;
import com.example.demo.dto.searchDto.MotelSearchDTO;
import com.example.demo.services.MotelService;
import com.example.demo.util.PaginationUtil;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/motel")
public class MotelController {
	
	@Autowired
	MotelService motelService;
	@PostMapping(value = "/doSearch")
	public ResponseEntity<Page<MotelResponDTO>> doSearch(@RequestBody MotelSearchDTO dto ,Pageable pageable){
		Page<MotelResponDTO> result= motelService.doSearch(dto, pageable);
		return new ResponseEntity<>(result, HttpStatus.OK);

	}
	@GetMapping(value = "/doSearchUser/{id}")
	public ResponseEntity<Page<MotelResponDTO>> doSearchuser(@PathVariable("id") String id ,Pageable pageable){
		Page<MotelResponDTO> result= motelService.doSearchUser(id, pageable);
		return new ResponseEntity<>(result, HttpStatus.OK);

	}
	
	@PostMapping(value = "/save")	
	public ResponseEntity<?> save(@RequestBody MotelDTO dto){
		return new ResponseEntity<>(motelService.save(dto), HttpStatus.OK);

	}
	
	
	@PostMapping(value = "/searchClient")
	public ResponseEntity<?> searchClient(@RequestBody MotelSearchClientDTO clientDTO){
		
		 		return new ResponseEntity<>(motelService.searchClient(clientDTO), HttpStatus.OK);

	}
	

}
