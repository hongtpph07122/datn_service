package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table ( name = "user_role")
public class UserRole {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column( name = "user_id")
	private Long user_id;
	@Column ( name = "role_id")
	private Long role_id;
	
	public UserRole(Long user_id, Long role_id) {
		this.user_id = user_id;
		this.role_id = role_id;
	}
}
