package com.example.demo.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.example.demo.converter.UserConverter;
import com.example.demo.dto.ParamSearchUserDTO;
import com.example.demo.dto.ResultDataDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.dto.UserResponseDTO;
import com.example.demo.entity.PasswordResetToken;
import com.example.demo.entity.User;
import com.example.demo.helper.ImageHelper;
import com.example.demo.payload.request.SetPasswordRequest;
import com.example.demo.repository.PasswordResetTokenRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.services.IFileStorageService;
import com.example.demo.services.IMailService;
import com.example.demo.services.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	UserRepository userRepository;
	@Autowired
	UserConverter userConverter;
	@Autowired
	PasswordEncoder passwordEncoder;
	@Autowired
	PasswordResetTokenRepository passwordResetTokenRepository;
	@Autowired
	IMailService mailSerivce;
	@Autowired
	JavaMailSender mailSender;
	@Autowired
	IFileStorageService storageService;

	@Override
	public UserDTO getUserById(Long id) {
		UserDTO userDTO = new UserDTO();
		User user = new User();
		user = userRepository.findOneById(id);
		userDTO = userConverter.toDTO(user);
		return userDTO;
	}
	
	// Update fullname, phone, adress, birthDay, avatar
	// Trả về 1 String
	@Override
	public String updateUser(UserDTO userDTO) {
		User user = null;
		String avatar = null;
		try {
			user = userRepository.getOne(userDTO.getId());
			userConverter.toEntityUpdate(userDTO, user);
			if (userDTO.getFile() != null) {
				if (!ImageHelper.hasImageFormat(userDTO.getFile())) {
					return "Ảnh đại diện phải là tệp tin ảnh";
				}
				avatar = storageService.save(userDTO.getFile(), user.getUsername(), "avatar");
				user.setAvatar(avatar);
			}
			user = userRepository.save(user);
			return "Cập nhật thành công";

		} catch (EntityNotFoundException e) {
			return "Cập nhật thất bại - Không tìm thấy User";
		} catch (Exception e) {
			return "Cập nhật thấy bại";
		}

	}

	// Thay đổi status User
	@Override
	public String changeStatus(UserDTO dto) {
		try {
			User user = userRepository.getOne(dto.getId());
			user.setStatus(dto.getStatus());
			user = userRepository.save(user);
			return "Thay đổi trạng thái User thành công";
		} catch (EntityNotFoundException e) {
			return "Thay đổi status thất bại - Không tìm thấy User";
		} catch (Exception e) {
			return "Thay đổi status thấy bại : " + e;
		}
	}

	// Thay đổi mật khẩu User
	@Override
	public String changePassword(UserDTO userDTO) {
		try {
			User user = userRepository.getOne(userDTO.getId());
			Boolean isPassword = passwordEncoder.matches(userDTO.getPassword(), user.getPassword());
			if (isPassword == false) {
				return "Mật khẩu cũ không chính xác";
			}
			user.setPassword(passwordEncoder.encode(userDTO.getNewpassword()));
			user = userRepository.save(user);
			return "Mật khẩu thay đổi thành công";
		} catch (EntityNotFoundException e) {
			return "Thay đổi mật khẩu thất bại: Not Found User";
		} catch (Exception e) {
			return "Thất bại: Exception " + e;
		}
	}

	// Xóa tài khoản
	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);

	}

	// Quên mật khẩu
	@Override
	public String forgotPassword(String email) {
		Boolean existEmail = userRepository.existsByEmail(email);
		if (existEmail == false) {
			return "Email của bạn không được đăng ký với chúng tôi";
		}
		User user = userRepository.findUserByEmail(email);
		String token = UUID.randomUUID().toString();
		this.createPasswordResetTokenForUser(user, token);
		try {
			SimpleMailMessage mail = mailSerivce.constructResetTokenEmail("http://localhost:8081/api", token, user,
					"Vui lòng click vào link để đặt lại mật khẩu : ");
			mailSender.send(mail);
			return " Chúng tôi đã gửi đến mail của bạn 1 đường dẫn, Vui lòng kiểm tra lại mail của bạn";
		} catch (Exception e) {
			return "Mail cài đặt lại mật khẩu gửi đi THẤT BẠI";
		}

	}

	public void createPasswordResetTokenForUser(User user, String token) {
		Calendar nowC = Calendar.getInstance();
		nowC.add(Calendar.HOUR, 2);
//		nowC.add(Calendar.MINUTE, 1);
		Date expiryDate = nowC.getTime();
		PasswordResetToken myToken = new PasswordResetToken(user, token, expiryDate);
		passwordResetTokenRepository.save(myToken);
	}

	@Override
	public String validatePasswordToken(String token) {
		PasswordResetToken passToken = passwordResetTokenRepository.findPasswordTokenForPasswordResetToken(token);
		return !isTokenFound(passToken) ? "Token không chính xác"
				: isTokenExpired(passToken) ? "Đường link đã hết hạn" : null;

	}

	private boolean isTokenFound(PasswordResetToken passToken) {
		return passToken != null;
	}

	private boolean isTokenExpired(PasswordResetToken passToken) {
		final Calendar cal = Calendar.getInstance();
		return passToken.getExpiryDate().before(cal.getTime());
	}

	@Override
	public String resetPassword(SetPasswordRequest request) {
		String result = validatePasswordToken(request.getToken());
		if (result != null) {
			return result;
		}
		try {
			Long user_id = passwordResetTokenRepository.findUserIdBytoken(request.getToken());
			User user = userRepository.getOne(user_id);
			user.setPassword(passwordEncoder.encode(request.getPassword()));
			user = userRepository.save(user);
			return "Mật khẩu của bản đã được thay đổi";
		} catch (Exception e) {
			return "Gặp lỗi, không thể đổi mật khẩu";
		}
	}

	// Tìm kiếm
	@Override
	public Page<UserResponseDTO> doSearch(ParamSearchUserDTO paramSearchUserDTO, Pageable pageable) {

		Page<UserResponseDTO> pageUser = userRepository.doSearch(
				paramSearchUserDTO.getUsername(),
				paramSearchUserDTO.getEmail(),
				paramSearchUserDTO.getStatus(), 
//				paramSearchUserDTO.getFromDate(),
//				paramSearchUserDTO.getToDate(), 
				paramSearchUserDTO.getRoleID(),
				pageable
				).map(this::objectToDTO);

		return pageUser;
	}

	public UserResponseDTO objectToDTO(Object[] object) {
		UserResponseDTO model = new UserResponseDTO();
		model.setId(safeToLong(object[0]));
		model.setUsername((String) object[1]);
		model.setFullname((String) object[2]);
		model.setEmail((String) object[3]);
		model.setStatus((int) object[4]);
		model.setRolename((String) object[5]);
	    model.setCreated_date( (Date) object[6]);
		return model;
	}

	public static Long safeToLong(Object obj1) {
		Long result = 0L;
		if (obj1 != null) {
			try {
				result = Long.parseLong(obj1.toString());
			} catch (Exception ex) {
			}
		}

		return result;
	}

}
