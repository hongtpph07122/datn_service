package com.example.demo.services;


import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.dto.ParamSearchUserDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.dto.UserResponseDTO;
import com.example.demo.payload.request.SetPasswordRequest;

public interface IUserService {
	// Lấy User thông qua ID
		UserDTO getUserById(Long id) ;
	// Update
	String updateUser(UserDTO userDTO);
	
	
	// Tìm kiếm Pageable pageable,
	Page<UserResponseDTO> doSearch(ParamSearchUserDTO dto, Pageable pageable);
	
	// Thay đổi trạng thái User
	String changeStatus( UserDTO userDTO);
	
	// Thay đổi mật khẩu của User
	String changePassword(UserDTO userDTO);
	
	//Quên mật khẩu
	String forgotPassword(String email);
	
	//Reset mật khẩu
	String validatePasswordToken(String Retoken);
	
	//
	String resetPassword(SetPasswordRequest request);
	
	
	
	// Xóa tài khoản
	void deleteUser(Long id);
}
