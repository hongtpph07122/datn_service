package com.example.demo.services.impl;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.services.IMailService;

@Service
public class MailServiceImpl implements IMailService{

	public SimpleMailMessage constructEmail(String subject, String body, User user) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setFrom("aimabiet9119@gmail.com");
		email.setSubject(subject);
		email.setText(body);
		email.setTo(user.getEmail());
		return email;
	}
	public SimpleMailMessage constructResetTokenEmail(String contextPath, String token, User user, String message) {
	    String url = contextPath + "/user/resetPassword?token=" + token;
	    return constructEmail("Reset Password", message + " \r\n" + url, user);
	}

}
