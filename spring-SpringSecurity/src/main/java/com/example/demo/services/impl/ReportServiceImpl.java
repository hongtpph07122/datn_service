package com.example.demo.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.searchDto.ReportResponDTO;
import com.example.demo.repository.ReportRepository;
import com.example.demo.services.ReportService;
import com.example.demo.util.DataUtil;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportRepository reportRepository;

	
	@Override
	public List<ReportResponDTO> doSearch(String motelId) {
		return reportRepository.doSearch(motelId).stream().map(objects->{
			ReportResponDTO dto  = new ReportResponDTO();
			dto.setUserId(DataUtil.safeToLong(objects[0]));
			dto.setUserName(DataUtil.safeToString(objects[1]));
			dto.setId(DataUtil.safeToLong(objects[2]));
			dto.setCreateDate(DataUtil.safeToString(objects[3]));
			dto.setMotelId(DataUtil.safeToLong(objects[4]));
			dto.setReason(DataUtil.safeToString(objects[5]));
			return dto;
		}).collect(Collectors.toList());
	}

}
