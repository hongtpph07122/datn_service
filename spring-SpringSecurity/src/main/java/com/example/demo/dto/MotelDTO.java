package com.example.demo.dto;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.example.demo.entity.Motel;
import com.example.demo.entity.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Component
public class MotelDTO {
	private Long motelId;
	private Long userId;
	private String title;
	private String description;
	private Integer motelType;
	private String longitude;
	private String latitude;
	private Double priceMin;
	private Double priceMax;
	private Double acreageMin;
	private Double acreageMax;
	private String deposits;
	private Integer uptopStatus;
	private String lastUptop;
	private String contactPhone;
	private String contactPerson;
	private String province;
	private String district;
	private String wards;
	private String street;
	private Integer liveWithHost;
	private Integer privateToilet;
	private Integer numberBedroom;
	private Integer numberToilet;
	private Integer floor;
	private Integer numberFloor;
	private Integer direction;
	private Integer apartmentSituation;
	private Double length;
	private Double width;
	private Integer statusMotel;
	private Integer countView;
	private Date createdDatel;
	private String image1;
	private String image2;
	private String image3;
	private String image4;

	
	
	

	public Motel toDoMotel(MotelDTO dto) {
		
		Motel motel = new Motel();
		
		motel.setCreated_date(new Date());
		motel.setStatus(1);
		motel.setUptop_status(0);
		motel.setId(dto.getMotelId());
		motel.setUser_id(dto.getUserId());
		motel.setTitle(dto.getTitle());
		motel.setDescription(dto.getDescription());
		motel.setMotel_type(dto.getMotelType());
		motel.setLongitude(dto.getLongitude());
		motel.setLatitude(dto.getLatitude());
		motel.setPrice_min(dto.getPriceMin());
		motel.setPrice_max(dto.getPriceMax());
		motel.setAcreage_min(dto.getAcreageMin());
		motel.setAcreage_max(dto.getAcreageMax());
		motel.setDeposits(dto.getDeposits());
		motel.setContact_phone(dto.getContactPhone());
		motel.setContact_person(dto.getContactPerson());
		motel.setProvince(dto.getProvince());
		motel.setDistrict(dto.getDistrict());
		motel.setWards(dto.getWards());
		motel.setStreet(dto.getStreet());
		motel.setLive_with_host(dto.getLiveWithHost());
		motel.setPrivate_toilet(dto.getPrivateToilet());
		motel.setNumber_bedroom(dto.getNumberBedroom());
		motel.setNumber_toilet(dto.getNumberToilet());
		motel.setFloor(dto.getFloor());
		motel.setNumber_floor(dto.getNumberFloor());
		motel.setDirection(dto.getDirection());
		motel.setApartment_situation(dto.getApartmentSituation());
		motel.setLength(dto.getLength());
		motel.setWidth(dto.getWidth());
		motel.setCount_view(dto.getCountView());
		return motel;
	}
	

	




	

}
