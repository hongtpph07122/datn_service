package com.example.demo.dto.searchDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MotelSearchDTO {
private String username;
private String motelType;
private String fromDate;
private String toDate;
private int status;
private String province;
private String district;
private String wards;

}
