package com.example.demo.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParamSearchUserDTO {
	private String username;
	private String email;
	private int status;
	private int roleID;
	private Date fromDate;
	private Date toDate;
	
}
