package com.example.demo.dto.searchDto;

import java.util.List;

import com.example.demo.entity.Photo;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class MotelSearchClientDTO {
private String province;
private String district;
private String wards;
private List<Integer> typeMotel;
private Double priceMin;
private Double priceMax;


}
