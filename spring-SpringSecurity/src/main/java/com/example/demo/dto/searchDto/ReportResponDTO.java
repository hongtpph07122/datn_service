package com.example.demo.dto.searchDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ReportResponDTO {
	private Long id;
	private String createDate;
	private Long motelId;
	private String reason;
	private Long userId;
	private String userName;
}
