package com.example.demo.converter;

import org.springframework.stereotype.Component;

import com.example.demo.dto.RoleDTO;
import com.example.demo.entity.Role;

@Component
public class RoleConverter {
	
	public RoleDTO toDTO( Role role) {
		RoleDTO dto = new RoleDTO();
		dto.setId(role.getId());
		dto.setName(role.getName());
		return dto;
	}
	
	public Role toEntity ( RoleDTO dto ) {
		Role role = new Role();
		role.setId(dto.getId());
		role.setName(dto.getName());
		return role;
	}
}
