package com.example.demo.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dto.RoleDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;

@Component
public class UserConverter {
	@Autowired
	RoleConverter roleConverter;
	public UserDTO toDTO(User user) {
		UserDTO dto = new UserDTO();
		dto.setId(user.getId());
		dto.setUsername(user.getUsername());
		dto.setPassword(user.getPassword());
		dto.setFullname(user.getFullname());
		dto.setEmail(user.getEmail());
		dto.setBirthday(user.getBirthday());
		dto.setAddress(user.getAddress());
		dto.setPhone(user.getPhone());
		dto.setAvatar(user.getAvatar());
		dto.setStatus(user.getStatus());
		dto.setBalance(user.getBalance());
		dto.setCreate_date(user.getCreated_date());
		dto.setGender(user.getGender());
		return dto;
	}
	
	public User toEntity( UserDTO dto) {
		User user = new User();
		user.setId(dto.getId());
		user.setUsername(dto.getUsername());
		user.setPassword(dto.getPassword());
		user.setFullname(dto.getFullname());
		user.setEmail(dto.getEmail());
		user.setBirthday(dto.getBirthday());
		user.setAddress(dto.getAddress());
		user.setPhone(dto.getPhone());
		user.setAvatar(dto.getAvatar());
		user.setStatus(dto.getStatus());
		return user;
	}
	
	public User toEntityUpdate( UserDTO userDTO, User user) {
		user.setFullname(userDTO.getFullname());
		user.setPhone(userDTO.getPhone());
		user.setAddress(userDTO.getAddress());
		user.setBirthday(userDTO.getBirthday());
		user.setGender(userDTO.getGender());
		return user;
	}
}
